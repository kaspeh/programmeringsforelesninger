# Når vi vil skrive ut tekst med verdier fra variabler i, så kan vi bruke flere måter:
#   1 Skrive de inn manuelt **
#   2 Bruke %-formattering **
#   3 Bruke .format() **
#   4 Konvertere verdiene til str, og plusse de sammen **
#   5 Sette sammen flere tekstsnutter og verdier med komma, direkte i printen (gir ekstra mellomrom hvor vi ikke alltid vil ha de)
#   6 Bruke f-string
# ** Vi skal ikke bruke de med "**", fordi de to siste er mer praktiske.
# f-strings er best. Nr. 5 _kan_ funke greit når en bare skal skrive ut noe kort. F.eks. print('penger:', penger)

# Oppgave: Vi skal skrive ut:
# 'Hei, jeg heter Paul, jeg har 100000000 kroner i banken, og kjører en Ferrari!'
# Når variablene er satt som følger: navn:Paul, penger:100_000_000, bil:Ferrari


# Endre disse variablene
navn = 'Paul'
penger = 100_000_000
bil = 'Ferrari'

# Alle måtene å løse problemet på.
#1 tekst = 'Hei, jeg heter Paul, jeg har 100000000 kroner i banken, og kjører en Ferrari!'
#2 tekst = 'Hei, jeg heter %s, jeg har %i kroner i banken, og kjører en %s' % (navn, penger, bil)
#3 tekst = 'Hei, jeg heter {}, jeg har {} kroner i banken, og kjører en {}!'.format(navn, penger, bil)
#4 tekst = 'Hei, jeg heter ' + navn + ', jeg har ' + str(penger) + ' kroner i banken, og kjører en ' + bil + '!'
#5 print('Hei, jeg heter ', navn, ', jeg har ', penger, ' kroner i banken, og kjører en ', bil, '!')

#6
tekst = f'Hei, jeg heter {navn}, jeg har {penger} kroner i banken, og kjører en {bil}!'

print(tekst)
