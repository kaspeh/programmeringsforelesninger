# Definere funksjonen
def les_fra_fil(filnavn):
    # Magi skjer her. Vi skal lære mer om det senere. :)
    with open(filnavn, 'r') as fil:
        tekst = fil.read()
    return tekst

# Kalle funksjonen og skrive ut resultat fra tekstfilen
dagbok = les_fra_fil('dagbok.txt')
print(dagbok)
